clear, close all

%% HMM Indoor localization (preliminary implementation)
% M. Yagci Date: 08.01.2014

% -------------------------------------------------------------------
%% Map initialization + Observations (for simulation purposes)
% -------------------------------------------------------------------

% Map size
nRows = 16; nCols = 4; 

% put some obstacles in the map
obstacles = [ 	1 2; 1 3; 2 3; 3 1; 5 2; 5 3; 5 4; 7 1; 7 2; 7 3; 8 2; 8 3; 
                10 3; 11 4; 12 1; 12 3; 14 2; 14 3; 15 2; 15 3; 15 4; 16 3];
sizeObstacles = size(obstacles,1);

% state neighborhood model
nDirs = 4; % (N,W,E,S) 
neighborhood = zeros(nRows,nCols,nDirs);

% discrete pdf representing state distribution
pdf = zeros(nRows,nCols);

% initialize
for i = 1:nRows
    for j=1:nCols
		pdf(i,j) = 1/(nRows*nCols - sizeObstacles);
		
		% mark obstacles for each neighborhood direction, e.g. [1 1 0 0] 
		if ( j+1 > nCols || sum(ismember(obstacles,[i j+1],'rows'))==1)
			neighborhood(i,j,1) = 1;
		end
		if ( j-1 < 1 || sum(ismember(obstacles,[i j-1],'rows'))==1)
			neighborhood(i,j,4) = 1;
		end
		if ( i+1 > nRows || sum(ismember(obstacles,[i+1 j],'rows'))==1)
			neighborhood(i,j,3) = 1;
		end
		if ( i-1 < 1 || sum(ismember(obstacles,[i-1 j],'rows'))==1)
			neighborhood(i,j,2) = 1;		
		end		
    end
end

% assumed error in AP detection
err = 0.2; 

% create random fingerprints by flipping 1 bit each time
% (this is silly, real data should be used)
nFPrints = 10; % number of APs
FPrints = zeros(nRows,nCols,nFPrints);
randomFP = ones(1,nFPrints);
for i = 1:nRows
    for j=1:nCols
        b = randi(nFPrints);
        randomFP(b) = 1-randomFP(b); % flip
        FPrints(i,j,:) = randomFP;
    end
end

% observed APs (some random fingerprints)
observations = [1 1 0 0 0 0 0 0 0 1; 
 				1 1 0 0 0 0 0 0 0 1;
 				1 0 0 0 1 1 0 0 0 0;
 				1 0 0 0 1 1 0 0 0 0;
                1 0 1 0 1 1 0 0 0 0;
                1 0 1 0 1 1 0 0 0 0];

				

% -------------------------------------------------------------------
%% Forward algorithm for filtering
% -------------------------------------------------------------------

% Initialize forward variables
for i = 1:nRows
	for j=1:nCols	
		if (sum(ismember(obstacles,[i j],'rows')) == 0)
        % not an obstacle
            FPFlattened = reshape(FPrints(i,j,:),1,nFPrints);
            d = sum(abs(FPFlattened-observations(1,:)));
			pdf(i,j) = pdf(i,j) * (1-err)^(nFPrints-d)*err^d;
        else
        % an obstacle
			pdf(i,j) = 0;
		end
	end	
end
alphas = pdf;

% normalized alphas to get the posterior
posterior = alphas/ sum(sum(alphas));
visualize_map
pause(1)


% Forward recursion
for o=2:size(observations,1)
    o
	currentObservation = observations(o,:);

	tempGrid = zeros(nRows,nCols); % holds intermediate scores to calculate forward variables (alphas)
	for i = 1:nRows
		for j=1:nCols
			if (sum(ismember(obstacles,[i j],'rows')) == 0)
				nv = [ neighborhood(i,j,1) neighborhood(i,j,2) neighborhood(i,j,3) neighborhood(i,j,4) ];

                % transition probability
                pTrans = 1/(nDirs+1-sum(nv));
                
                % self update
                tempGrid(i,j) = ...
                        tempGrid(i,j) + pTrans * alphas(i,j);
                
				% neighbors update
                if nv(1) == 0
					tempGrid(i,j+1) = ...
                        tempGrid(i,j+1) + pTrans * alphas(i,j);
                end                
				if nv(2) == 0
					tempGrid(i-1,j) = ...
                        tempGrid(i-1,j) + pTrans * alphas(i,j);
				end
				if nv(3) == 0
					tempGrid(i+1,j) = ...
                        tempGrid(i+1,j) + pTrans * alphas(i,j);
				end
				if nv(4) == 0
					tempGrid(i,j-1) = ...
                        tempGrid(i,j-1) + pTrans * alphas(i,j);
				end
			end			
		end
    end
    
    for i=1:nRows
        for j=1:nCols
			if (sum(ismember(obstacles,[i j],'rows')) == 0)
                FPFlattened = reshape(FPrints(i,j,:),1,nFPrints);
                d = sum(abs(FPFlattened-currentObservation));
				b = (1-err)^(nFPrints-d)*err^d;
				alphas(i,j) = tempGrid(i,j) * b;
			else
				alphas(i,j) = 0;
			end
        end
    end
    
    % normalized alphas to get the posterior
    posterior = alphas/ sum(sum(alphas));
    visualize_map
    pause(1)
    
end
